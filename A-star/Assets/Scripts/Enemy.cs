﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {
    [SerializeField]
    public List<Node> ways;
    public int Index;
    public int Damage;
    // Use this for initialization
    void Start () {
		//ways = new List<Node>();
        Index = 0;
	}
    private void Awake() {
        GetTheWay();
    }

    // Update is called once per frame
    void Update () {
        GetTheWay();
		FollowPath(ways);
        if (!GetComponent<Health>().IsAlive())
        {
            Destroy(gameObject);
        }
            
	}

    public void FollowPath(List<Node> paths) {
        if (paths.Count > 0)
        {
            if (Index <= paths.Count)
            {
                Vector3 currentPath = paths[Index].Position;
                if (Vector3.Distance(transform.position, currentPath) > 0.3f)
                {
                    transform.LookAt(currentPath);
                    transform.position = Vector3.MoveTowards(transform.position, currentPath, 10f * Time.deltaTime);
                }
                else
                {
                    if (Index <= paths.Count)
                        Index++;
                }
            }
        }
    }
    public void GetTheWay()
    {
        ways = GameObject.Find("GameManager").GetComponent<Pathfinding>().FinalPath;
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "End")
        {
            DestroyObject(gameObject);
            collision.gameObject.GetComponent<Health>().TakeDamage(Damage);
        }
    }
}
