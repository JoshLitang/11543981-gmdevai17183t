﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemy : MonoBehaviour {
    public GameObject spawn;
    public float SpawnInterval;
    public float WaveInterval;
    
    private GameObject enemy;
    private int index;
    private List<GameObject> wave;
    private float spawnInterval;
    private float spawnMaxInterval;
    private float waveInterval;
	// Use this for initialization
	void Start () {
        index = 0;
        //Deploy();
        spawnInterval = 0;
        spawnMaxInterval = 1;
	}
	
	// Update is called once per frame
	void Update () {
        spawnInterval += Time.deltaTime;

        if (spawnInterval >= spawnMaxInterval && index < 10)
        {
            Deploy();
            spawnInterval = 0;
            index++;
            //&& wave == null
            
        }
        if (index == 10)
            spawnMaxInterval = 5;
        else
        {
            spawnMaxInterval = 1;
        }

        if (spawnInterval >= spawnMaxInterval && index >= 10 && index < 20)
        {
            Deploy();
            spawnInterval = 0;
            index++;
        }

    }
    private void Deploy() {
        enemy = Instantiate(spawn, transform.position, Quaternion.identity);
        //wave.Add(enemy);
        
        Debug.Log("Spawn traps called");
    }
}
