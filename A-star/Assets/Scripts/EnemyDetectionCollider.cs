﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDetectionCollider : MonoBehaviour {
    public Tower Tower;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider other) {
        Debug.Log(other.gameObject.tag);
        if (other.tag == "Enemy") {
            Tower.Target.Add(other.gameObject);
            Tower.CurrState = Tower.State.Firing;
        }
    }

    void OnTriggerExit(Collider other) {
        if (other.tag == "Enemy")
            Tower.Target.Remove(other.gameObject);
    }
}
