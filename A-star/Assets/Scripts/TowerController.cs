﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        CheckForClickState();
	}

    void CheckForClickState()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                /*print("Hit something!");
                Debug.DrawLine(ray.origin, hit.point);
                Debug.Log(hit.collider.gameObject.tag);
                Debug.Log(hit.collider.gameObject.name);
                */
                if (hit.collider.gameObject.tag == "TowerSpawn")
                {
                    hit.collider.gameObject.gameObject.GetComponent<SpawnTower>().Spawn();
                }
            }
        }
    }

}
