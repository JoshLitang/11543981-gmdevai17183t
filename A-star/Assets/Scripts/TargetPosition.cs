﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetPosition : MonoBehaviour {

    public Health health;
	// Use this for initialization
	void Start () {
        health = GetComponent<Health>();
	}
	
	// Update is called once per frame
	void Update () {
        if (!health.IsAlive()){
            Destroy(gameObject);
        }
	}

    void OnTriggerEnter(Collider other) {
        if (other.tag == "Enemy") {
            //GetComponent<Health>().TakeDamage(other.GetComponent<Enemy>().Damage);
        }
    }
}
