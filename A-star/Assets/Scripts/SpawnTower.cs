﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnTower : MonoBehaviour {

    public GameObject Tower;

    private GameObject tower;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Spawn() {
        tower = Instantiate( Tower, transform.position, Quaternion.identity);
    }
}
