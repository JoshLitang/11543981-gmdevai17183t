﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {

    public float MaxHealth;
    public float CurrHealth;
    // Use this for initialization
    void Start() {
        CurrHealth = MaxHealth;
    }

    public void TakeDamage(float damage) {
        CurrHealth -= damage;
        if (damage > CurrHealth)
            CurrHealth = 0;
        //Debug.Log(currhealth);
    }

    public void Heal(float healing) {
        CurrHealth += healing;
        if (CurrHealth > MaxHealth && IsAlive())
            CurrHealth = MaxHealth;
        //Debug.Log(currhealth);
    }

    public bool IsAlive() {
        return CurrHealth > 0;
    }
}
