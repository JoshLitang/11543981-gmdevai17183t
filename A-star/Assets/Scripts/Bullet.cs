﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public float Damage;
    public GameObject Target;
	// Use this for initialization
	void Start () {
        Despawn();
	}
	
	// Update is called once per frame
	void Update () {
        if (Target != null)
            transform.position = Vector3.MoveTowards(transform.position, Target.transform.position, Time.deltaTime *10);
	}

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            DestroyObject(gameObject);
            collision.gameObject.GetComponent<Health>().TakeDamage(Damage);
        }
    }

    private void Despawn() {
        DestroyObject(gameObject, 1);
    }
}
