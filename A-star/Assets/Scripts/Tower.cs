﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour {
    public GameObject Bullet;
    public List<GameObject> Target;
    public State CurrState;
    public enum State
    {
        Waiting,
        Firing
    };

    private GameObject projectile;
    
    // Use this for initialization
    void Start () {
        CurrState = State.Waiting;
	}
	
	// Update is called once per frame
	void Update () {
		if (CurrState == State.Firing)
        {
            Shoot();
        }

        if (Target.Count < 1)
        {
            CurrState = State.Waiting;
        }
	}

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.gameObject.tag);
    }

    void Shoot() {
        foreach (GameObject enemy in Target) {
            if (enemy != null)
            {
                transform.LookAt(enemy.transform.position);
                projectile = Instantiate(Bullet, transform.position, transform.rotation);
                projectile.GetComponent<Bullet>().Target = enemy;
            }
        }
    }
}
