﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {
    [SerializeField]
    public List<Node> ways;
    public int index;
    // Use this for initialization
    void Start () {
		//ways = new List<Node>();
        index = 0;
	}
    private void Awake() {
        GetTheWay();
    }

    // Update is called once per frame
    void Update () {
        GetTheWay();
		FollowPath(ways);
	}

    public void FollowPath(List<Node> paths) {
        if (paths.Count > 0)
        {
            if (index <= paths.Count)
            {
                Vector3 currentPath = paths[index].Position;
                if (Vector3.Distance(transform.position, currentPath) > 0.3f)
                {
                    transform.LookAt(currentPath);
                    transform.position = Vector3.MoveTowards(transform.position, currentPath, 10f * Time.deltaTime);
                }
                else
                {
                    if (index <= paths.Count)
                        index++;
                }
            }
        }
    }
    public void GetTheWay()
    {
        ways = GameObject.Find("GameManager").GetComponent<Pathfinding>().FinalPath;
    }
}
