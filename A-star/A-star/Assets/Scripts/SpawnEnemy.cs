﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemy : MonoBehaviour {
    public GameObject spawn;
    public float SpawnInterval;
    public float WaveInterval;
    
    private GameObject enemy;
	// Use this for initialization
	void Start () {
		InvokeRepeating("Deploy", 1, SpawnInterval);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private void Deploy() {
        Debug.Log("Spawn traps called");
        enemy = Instantiate(spawn, transform.position, Quaternion.identity);
    }
}
