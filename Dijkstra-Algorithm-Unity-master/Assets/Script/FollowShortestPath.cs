﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowShortestPath : MonoBehaviour {

    public List<Transform> ways; //{ get; set; }
    public int index;
    // Use this for initialization
    void Start () {
        ways = new List<Transform>();
        index = 0;
        //StartCoroutine(FollowPath(ways));
    }
	
	// Update is called once per frame
	void Update () {
        FollowPath(ways);
	}

    //public void StartMoving(List<Transform> ways)
    //{
    //    this.ways = ways;
    //    StartCoroutine(FollowPath(ways));
    //}

    public void FollowPath(List<Transform> paths)
    {
        /*
        if (paths != null) {
            foreach (Transform path in paths) {
                transform.position = path.position;
                yield return new WaitForSeconds(1f);
            }
        }
        */
        if (paths.Count > 0)
        {
            if (index <= paths.Count)
            {
                Vector3 currentPath = paths[index].transform.position;
                if (Vector3.Distance(transform.position, currentPath) > 0.3f)
                {
                    transform.LookAt(currentPath);
                    transform.position = Vector3.MoveTowards(transform.position, currentPath, 10f * Time.deltaTime);
                }
                else
                {
                    if (index <= paths.Count)
                        index++;
                }
            }
            //transform.position = paths[0].transform.position;
            //foreach (Transform path in paths)
            //{
            //    //Traveler.GetComponent<Transform>().position = Vector3.Lerp(Traveler.GetComponent<Transform>().position, path.position, 1f* Time.deltaTime);
            //    //Traveler.GetComponent<Transform>().position = path.transform.position;
            //    transform.position = Vector3.MoveTowards(transform.position, path.position, 100f * Time.deltaTime);
            //    //transform.position = path.transform.position;
            //    yield return new WaitForSeconds(1f);
            //}
            
        }
    }

}
